// import react module
import React from 'react';
// import navigation
import { AppContainer } from './src/routes/appRoute';

export default class App extends React.Component {
  render() {
    return (
      <AppContainer />
    );
  }
}
