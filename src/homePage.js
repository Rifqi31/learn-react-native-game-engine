/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';

export default class homePage extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.textStyle}>Example React Native Game Engine</Text>
        <View style={styles.buttonSpace}>
          <Button title="Example 1: Box Falling" onPress={() => navigation.navigate('Menu_Example1')}/>
        </View>
        <View style={styles.buttonSpace}>
          <Button title="Example 2: Simple Rectangle" onPress={() => navigation.navigate('Menu_Example2')}/>
        </View>
        <View style={styles.buttonSpace}>
          <Button title="Example 3: Click Create Box" onPress={() => navigation.navigate('Menu_Example3')}/>
        </View>
        <View style={styles.buttonSpace}>
          <Button title="Example 4: Upload Asset File" onPress={() => navigation.navigate('Menu_Example4')}/>
        </View>
        <View style={styles.buttonSpace}>
          <Button title="Example 5: Adding System" onPress={() => navigation.navigate('Menu_Example5')}/>
        </View>
        <View style={styles.buttonSpace}>
          <Button title="Example 5: Donkey Kong" onPress={() => navigation.navigate('Menu_Example6')}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  buttonSpace: {
    flexDirection: 'column',
    marginVertical: 15
  },
  textStyle: {
    fontSize: 20,
    fontWeight: 'bold'
  }
});
