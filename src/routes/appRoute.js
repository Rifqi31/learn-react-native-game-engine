// import react module
import React from 'react'; 
// import react navigation
import { createStackNavigator, createAppContainer } from 'react-navigation';
// import homePage
import Home from '../homePage';
// import example react native game engine
import Example1 from '../games/menu1/bouncingBox/index';
import Example2 from '../games/menu2/creatingAssets/index';
import Example3 from '../games/menu3/creatingPhysics/index';
import Example4 from '../games/menu4/uploadingAssets/index';
import Example5 from '../games/menu5/addingPhysics/index';
import Example6 from '../games/menu6/src/index';

const createMenu = createStackNavigator({
    HomePage : { screen : Home },
    Menu_Example1 : { screen : Example1 },
    Menu_Example2 : { screen : Example2 },
    Menu_Example3 : { screen : Example3 },
    Menu_Example4 : { screen : Example4 },
    Menu_Example5 : { screen : Example5 },
    Menu_Example6 : { screen : Example6 },

}, {
    initialRouteName: 'HomePage',
    headerMode : 'none'
});

export const AppContainer = createAppContainer(createMenu);
