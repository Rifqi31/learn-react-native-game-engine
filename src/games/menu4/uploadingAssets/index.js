import React, { PureComponent } from "react";
import { StyleSheet, Dimensions } from "react-native";
import { GameEngine } from "react-native-game-engine";
import Matter from "matter-js";
// import game asset
import Platform from "./components/platforms";
import Mario from "./components/mario";

// import Tile from "./components/common/tile";
// import { collusionCategories } from "./components/utils/constants";

//-- Overriding this function because the original references HTMLElement
Matter.Common.isElement = () => false;

const { width, height } = Dimensions.get("window");
const scale = Math.min(width, 430) / 375;
const cx = width / 2;
const cy = height / 2;
const offsetY = (height - 465) / 2 - 35;
const platformWidth = Math.min(width, 430);

let engine = Matter.Engine.create({ enableSleeping: false });
let world = engine.world;

export default class App extends PureComponent {
  render() {
    return (
      <GameEngine
        style={styles.container}
        // systems={Systems}
        entities={{
          physics: { engine: engine, world: world },
          platform1: Platform(
            world,
            { x: cx, y: offsetY + 35 },
            0,
            platformWidth * 0.25
          ),

          platform2: Platform(
            world,
            { x: cx - 100, y: offsetY + 100 },
            0,
            platformWidth * 0.25
          ),

          platform3: Platform(
            world,
            { x: cx + 100, y: offsetY + 100 },
            0,
            platformWidth * 0.25
          ),

          platform4: Platform(
            world,
            { x: cx , y: offsetY + 180 },
            0,
            platformWidth * 0.25
          ),

          // long platform
          platform5: Platform(
            world,
            { x: cx , y: offsetY + 250 },
            0,
            platformWidth * 0.25
          ),

          platform6: Platform(
            world,
            { x: cx - 60 , y: offsetY + 250 },
            0,
            platformWidth * 0.25
          ),

          platform7: Platform(
            world,
            { x: cx + 60 , y: offsetY + 250 },
            0,
            platformWidth * 0.25
          ),
          //

          platform8: Platform(
            world,
            { x: cx + 90 , y: offsetY + 320 },
            0,
            platformWidth * 0.25
          ),

          platform9: Platform(
            world,
            { x: cx - 90 , y: offsetY + 320 },
            0,
            platformWidth * 0.25
          ),

          platform10: Platform(
            world,
            { x: cx , y: offsetY + 400 },
            0,
            platformWidth * 0.25
          ),

          // long platform
          platform11: Platform(
            world,
            { x: cx , y: offsetY + 470 },
            0,
            platformWidth * 0.25
          ),

          platform12: Platform(
            world,
            { x: cx - 90 , y: offsetY + 470 },
            0,
            platformWidth * 0.25
          ),

          platform13: Platform(
            world,
            { x: cx + 90 , y: offsetY + 470 },
            0,
            platformWidth * 0.25
          ),
          //

          // long platform
           platform14: Platform(
            world,
            { x: cx , y: offsetY + 490 },
            0,
            platformWidth * 0.25
          ),

          platform15: Platform(
            world,
            { x: cx - 90 , y: offsetY + 490 },
            0,
            platformWidth * 0.25
          ),

          platform16: Platform(
            world,
            { x: cx + 90 , y: offsetY + 490 },
            0,
            platformWidth * 0.25
          ),
          // -----------

          // fck damn player
          mario: Mario(world, { x: cx, y: offsetY + 470 - 20 / 2 - 20 }),

          // another way to upload the asset into the game
          // oil: {
          //   source: require("./src/components/props/oil.gif"),
          //   position: { x: cx - 140 * scale, y: offsetY + 427 },
          //   size: { width: 30, height: 56 },
          //   renderer: <Tile />
          // },
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "powderblue"
  }
});
