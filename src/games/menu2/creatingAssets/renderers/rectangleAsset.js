import React, { PureComponent } from "react";
import { View, StyleSheet } from "react-native";
import { array, object, string } from 'prop-types';

export default class rectanglesAsset extends PureComponent {
  render() {

      const widthAsset = this.props.size[0]
      const heightAsset = this.props.size[1]

      const posX = this.props.position[0]
      const posY = this.props.position[1]

    return(
        <View style={[
            stylesComponet.container,
            {
                left: posX,
                top: posY,
                width : widthAsset,
                height : heightAsset,
                backgroundColor : this.props.color
            }
        ]}/>
    );
  }
}

const stylesComponet = StyleSheet.create({
  container: {
    position: "absolute"
  }
});

rectanglesAsset.propTypes = {
  size: array,
  position : array,
  color: string
}
