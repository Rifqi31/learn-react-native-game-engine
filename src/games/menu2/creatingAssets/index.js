import React, { PureComponent } from "react";
import { StyleSheet, StatusBar, Dimensions } from "react-native";
import Matter from "matter-js";
import { GameEngine } from "react-native-game-engine"; // this is for scene and interaction with fck damn complecity
// import { GameLoop } from "react-native-game-engine"      // this is for simple scene and interaction only
// shape
import rectangleAsset from "./renderers/rectangleAsset";

// identify width and high from the phone screen
const { widthScreen, heightScreen } = Dimensions.get("screen");
// example and fck damn learn
const posObjectX = widthScreen / 2;
const posObjectY = heightScreen / 2;

// shape
const rectangleShape = Matter.Bodies.rectangle();

// Physics
const engine = Matter.Engine.create({ enableSleeping : false});
const world = engine.world;
Matter.World.add(world, [rectangleShape]);

const Physics = (entities, { time }) => {
  let engine = entities["physics"].engine;
    Matter.Engine.update(engine, time.delta);
    return entities;
}

export default class App extends PureComponent {
  render() {
    return(
      <GameEngine
        styles={styleFromAsset.container}
        systems={[Physics]}
        entities={{
          // fck damn it physic
          physics: { engine: engine, world: world },
          // rectangle
          shapeBox1 : { body: rectangleShape, size :[100, 100], position :[130, 120], color: "red", renderer: rectangleAsset },
        }}
      >
        <StatusBar hidden={true} />
      </GameEngine>
    );
  }
}


const styleFromAsset = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  }
})