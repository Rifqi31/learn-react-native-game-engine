import React, { PureComponent } from "react";
import { View, StyleSheet } from "react-native";
import { array, object, string } from "prop-types";

export default class rectangleAsset extends PureComponent {
  render() {
    // init variables
    // width and hight asset
    const widthRectangel = this.props.size[0];
    const heightRectangel = this.props.size[1];

    // const width = this.props.size[0];
    // const height = this.props.size[1];

    // position using coordinate x and y
    const coordinate_x = this.props.body.position.x - widthRectangel / 2;
    const coordinate_y = this.props.body.position.y - heightRectangel / 2;

    const colorBackgroundAsset = this.props.color;

    return (
      <View
        style={[
          stylesComponet.container,
          {
            left: coordinate_x,
            top: coordinate_y,
            width: widthRectangel,
            height: heightRectangel,
            backgroundColor: colorBackgroundAsset
          }
        ]}
      />
    );
  }
}

// styling
const stylesComponet = StyleSheet.create({
  container: {
    position: "absolute"
  }
});

// props type
// var : datatype
rectangleAsset.propTypes = {
  size: array,
  body: object,
  color: string
};
