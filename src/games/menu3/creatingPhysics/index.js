import React, { PureComponent } from "react";
import { StatusBar, StyleSheet, Dimensions } from "react-native";
import Matter from "matter-js";
import { GameEngine } from "react-native-game-engine";
import rectangelAsset from "./renderers/rectangleAsset";

//-- Overriding this function because the original references HTMLElement
Matter.Common.isElement = () => false;

// width and height is fack damn default variable
/* *** */
// dimension
const { width, height } = Dimensions.get("screen");
const scale = Math.min(width, 430) / 375;

// coordinate x and y asset
const default_assetPositionX = width / 2;
const default_assetPositionY = height / 2;

const offsetY = (height - 465) / 2 - 35;
const platformWidth = Math.min(width, 430);

// // physics
const engine = Matter.Engine.create({ enableSleeping: false });
const world = engine.world;
// /* *** */

// /* *** */
// positon x and y for rectangle
const rectanglePosX = default_assetPositionX;
const rectanglePosY = default_assetPositionY - 250;

// // height and width rectangle
const rectangleWidth = 80;
const rectangleHeight = 80;

// // call the fck damn shape
// rectangle
const rectangleShape = Matter.Bodies.rectangle(
  // x, y
  rectanglePosX,
  rectanglePosY,
  // width and height
  rectangleWidth,
  rectangleHeight
);
// /* *** */

// /* *** */
// position x and y for platform
const floorPosX = default_assetPositionX;
const floorPosY = offsetY + 510;

// height and width floor
const floorWidth = platformWidth;
const floorHeight = 60;

const rectanglePlatformShape = Matter.Bodies.rectangle(
  // x, y
  floorPosX,
  floorPosY,
  // width and height
  floorWidth,
  floorHeight,
  // options
  { isStatic: true }
);
// /* *** */

// // wrapping into the world
Matter.World.add(world, [rectangleShape, rectanglePlatformShape]);


const Physics = (entities, { time }) => {
  let engine = entities["physics"].engine;
  Matter.Engine.update(engine, time.delta);
  return entities;
};

let boxIds = 0;
const CreateBox = (entities, { touches, screen }) => {
  let world = entities["physics"].world;
  
  let rectangleWidth = 80;
  let rectangleHeight = 80;

  touches.filter(t => t.type === "press").forEach(t => {
      let rectangleShape = Matter.Bodies.rectangle(
        // x, y
        t.event.pageX,
        t.event.pageY,
        // width and height
        rectangleWidth,
        rectangleHeight
      );

      Matter.World.add(world, [rectangleShape]);

      entities[++boxIds] = {
        body: rectangleShape,
        size: [rectangleWidth, rectangleHeight],
        color: boxIds % 2 == 0 ? "pink" : "#B8E986",
        renderer: rectangelAsset
      };
  });
  return entities;
}

export default class App extends PureComponent {
  render() {
    return (
      <GameEngine
        styles={styleAsset.container}
        systems={[Physics, CreateBox]}
        entities={{
          physics: { engine: engine, world: world },
          rectangleShape: {
            body: rectangleShape,
            size: [rectangleWidth, rectangleHeight],
            color: "red",
            renderer: rectangelAsset
          },
          rectanglePlatformShape: {
            body: rectanglePlatformShape,
            size: [floorWidth, floorHeight],
            color: "green",
            renderer: rectangelAsset
          }
        }}
      >
        <StatusBar hidden={true} />
      </GameEngine>
    );
  }
}

const styleAsset = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  }
});
