import React from "react";
import { StyleSheet, Dimensions, StatusBar } from "react-native";
import { GameEngine, DefaultTouchProcessor } from "react-native-game-engine";
import Matter from "matter-js";
// import game asset
import Platform from "./src/components/platforms";
import Mario from "./src/components/mario";

// import game system
import Systems from "./src/systems";

//-- Overriding this function because the original references HTMLElement
Matter.Common.isElement = () => false;

const { width, height } = Dimensions.get("window");
const scale = Math.min(width, 430) / 375;
const cx = width / 2;
const cy = height / 2;
const offsetY = (height - 465) / 2 - 35;
const platformWidth = Math.min(width, 430);

let engine = Matter.Engine.create({ enableSleeping: false });
let world = engine.world;

export default class App extends React.PureComponent {
  render() {
    return (
      <GameEngine
        // style background color the game
        style={styles.container}
        // system for logic game works and physics law in the game
        systems={Systems}
        // entities for asset file, position and coordinates
        touchProcessor={DefaultTouchProcessor({
          triggerPressEventBefore: 150,
          triggerLongPressEventAfter: 151
        })}
        entities={{
          physics: { engine: engine, world: world },

          platform1: Platform(
            world,
            { x: cx, y: offsetY + 35 },
            0,
            platformWidth * 0.25
          ),

          platform2: Platform(
            world,
            { x: cx - 100, y: offsetY + 100 },
            0,
            platformWidth * 0.25
          ),

          platform3: Platform(
            world,
            { x: cx + 100, y: offsetY + 100 },
            0,
            platformWidth * 0.25
          ),

          platform4: Platform(
            world,
            { x: cx , y: offsetY + 180 },
            0,
            platformWidth * 0.25
          ),

          // long platform
          platform5: Platform(
            world,
            { x: cx , y: offsetY + 250 },
            0,
            platformWidth * 0.25
          ),

          platform6: Platform(
            world,
            { x: cx - 60 , y: offsetY + 250 },
            0,
            platformWidth * 0.25
          ),

          platform7: Platform(
            world,
            { x: cx + 60 , y: offsetY + 250 },
            0,
            platformWidth * 0.25
          ),
          //

          platform8: Platform(
            world,
            { x: cx + 90 , y: offsetY + 320 },
            0,
            platformWidth * 0.25
          ),

          platform9: Platform(
            world,
            { x: cx - 90 , y: offsetY + 320 },
            0,
            platformWidth * 0.25
          ),

          platform10: Platform(
            world,
            { x: cx , y: offsetY + 400 },
            0,
            platformWidth * 0.25
          ),

          // long platform
          platform11: Platform(
            world,
            { x: cx , y: offsetY + 470 },
            0,
            platformWidth * 0.25
          ),

          platform12: Platform(
            world,
            { x: cx - 90 , y: offsetY + 470 },
            0,
            platformWidth * 0.25
          ),

          platform13: Platform(
            world,
            { x: cx + 90 , y: offsetY + 470 },
            0,
            platformWidth * 0.25
          ),
          //

          // long platform
           platform14: Platform(
            world,
            { x: cx , y: offsetY + 490 },
            0,
            platformWidth * 0.25
          ),

          platform15: Platform(
            world,
            { x: cx - 90 , y: offsetY + 490 },
            0,
            platformWidth * 0.25
          ),

          platform16: Platform(
            world,
            { x: cx + 90 , y: offsetY + 490 },
            0,
            platformWidth * 0.25
          ),
          // -----------

          // fck damn player
          mario: Mario(world, { x: cx, y: offsetY + 470 - 20 / 2 - 20 })
        }}
      >
        <StatusBar hidden={true} />
      </GameEngine>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "powderblue"
  }
});
