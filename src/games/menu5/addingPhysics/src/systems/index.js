import Input from "./input";
import Control from "./control";
import Steering from "./steering";
import Platforms from "./platforms";
import Physics from "./physics";

export default [Input, Control, Steering, Platforms];
